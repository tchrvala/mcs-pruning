/*
 * Utils.h
 *
 *  Created on: Jan 11, 2010
 *      Author: rgreen
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

//#include <gsl/gsl_rng.h>
//#include <gsl/gsl_randist.h>

#ifndef _OPENMP
    #include "omp.h"
#endif

#include "anyoption.h"
#include "Bus.h"
#include "defs.h"
#include "Generator.h"
#include "Line.h"
#include "MTRand.h"
#include "Primes.h"
#include "RandomNumbers.h"

namespace Utils {

    /******************************** Vector ********************************/
    extern void printVector(std::vector < std::vector < double > >& v, std::string title, int precision = 2);
    extern void printVector(std::vector < std::vector < std::vector < double > > >& v, std::string title, int precision = 2);
    /******************************** End Vector ********************************/

   

    /******************************** String ********************************/
    extern std::string vectorToString(std::vector<double> v);
    extern std::string vectorToString(std::vector<int> v);
    extern std::string arrayToString(int* v, int size);
    extern void tokenizeString(std::string str,std::vector<std::string>& tokens,const std::string& delimiter );
    /******************************** End String ********************************/

    /******************************** Math ********************************/
    extern double sigMoid(double v);
    extern int factorial(int n);
    extern int combination(int n, int r);
    extern void twoByTwoCholeskyDecomp(std::vector<std::vector<double> >& A);
    extern double unitStep(double X);
    extern std::vector< std::vector<int> > matMult(std::vector< std::vector<int> > A, std::vector< std::vector<int> > B);
    extern double expm (double p, double ak);
    extern double series(int m, int id);
    extern double piNumber(int sampleNumber);
    extern std::vector<double> decToBin(double n, int numDigits);

    /******************************** End Math ********************************/

    /******************************** System ********************************/
    extern void loadSystemData(double& pLoad, double& qLoad, int& nb, int& nt, std::string curSystem, std::vector<Generator>& gens, std::vector<Line>& lines, std::vector<Bus>& Buses);

    /******************************** End System ********************************/

    /******************************** Logging ********************************/
    extern void writeLineOutages(std::string root, std::string curSystem, std::string method, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj,
            bool useLines, bool multiObj, char* aTime,
            bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
            std::vector < std::vector < int > > lineOutageCounts, std::vector<Line> lines, int numThreads);

    extern void writeGeneratorOutages(std::string root, std::string curSystem, std::string method, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj, bool useLines, bool multiObj, char* aTime,
            bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
            std::vector < std::vector < int > > genOutageCounts, std::vector<Generator> gens, int numThreads);

    extern std::string getBaseFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj,
            bool useLines, bool multiObj, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
            int numThreads);
                                
    extern std::string getFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj,
            bool useLines, bool multiObj, char* aTime, bool useLocalSearch, bool usePHEVs, bool negateFitness,
            double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,int numThreads);
                    
    //extern char* getTimeStamp();
    extern void getTimeStamp(char* aTime);
    /******************************** End Logging ********************************/

    /******************************** PHEV ********************************/
    extern void calculatePHEVLoad(
            double penetrationLevel, double rho,
            int totalVehicles, int numBuses,
            std::vector<double>& phevLoad, std::vector<double>& phevGen, MTRand& mt, pruning::PHEV_PLACEMENT PHEV_PLACEMENT = pruning::PP_EVEN_ALL_BUSES);

    /******************************** End PHEV ********************************/
    
    extern std::string getBooleanString             (bool value);
    extern std::string getPruningMethodString	 	(pruning::PRUNING_METHOD 		pm);
    extern std::string getClassificationMethodString(pruning::CLASSIFICATION_METHOD cm);
    extern std::string getSamplingMethodString		(pruning::SAMPLING_METHOD 		ps);
    extern std::string getPHEVPlacementString		(pruning::PHEV_PLACEMENT  		pp);
    extern std::string getStoppingMethodString		(pruning::STOPPING_METHOD 		sm);
    extern std::string getPruningObjString		    (pruning::PRUNING_OBJECTIVE 	po);
    
    extern pruning::PRUNING_METHOD        getPruningMethod		  (std::string s);
    extern pruning::CLASSIFICATION_METHOD getClassificationMethod (std::string s);
    extern pruning::SAMPLING_METHOD       getSamplingMethod		  (std::string s);
    extern pruning::PHEV_PLACEMENT        getPHEVPlacement		  (std::string s);
    extern pruning::STOPPING_METHOD       getStoppingMethod		  (std::string s);
    extern pruning::PRUNING_OBJECTIVE     getPruningObj           (std::string s);

    /******************************** Command Line ********************************/
    extern void setUsage(AnyOption* opt);
    extern void setOptions(AnyOption* opt);
    /******************************** EndCommand Line ********************************/
};

#endif /* UTILS_H_ */
