    // N - Number of points
    // D - Dimension
    std::vector < std::vector < double > > sobol_points(unsigned N, unsigned D){
        std::vector < std::vector < double > > POINTS(N, vector<double>(D, 0.0));
        ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-other-4.5600", ios::in);
        if(!infile){
            cout << "Input file containing direction numbers cannot be found!\n";
            exit(1);
        }
        char buffer[1000];
        infile.getline(buffer, 1000,'\n');
  
        // L = max number of bits needed 
        unsigned L = (unsigned)ceil(log((double)N)/log(2.0)); 

        // C[i] = index from the right of the first zero bit of i
        unsigned *C = new unsigned [N];
        C[0] = 1;
        for (unsigned i=1;i<=N-1;i++) {
            C[i] = 1;
            unsigned value = i;
            while (value & 1) {
                value >>= 1;
                C[i]++;
            }
        }
        // ----- Compute the first dimension -----
  
        // Compute direction numbers V[1] to V[L], scaled by pow(2,32)
        unsigned *V = new unsigned [L+1]; 
        for (unsigned i=1;i<=L;i++){ 
            V[i] = 1 << (32-i);
            // cout << L << " " << N << " " << i << ": " << V[i] << "\n";
        } // all m's = 1

        // Evalulate X[0] to X[N-1], scaled by pow(2,32)
        unsigned *X = new unsigned [N];
        X[0] = 0;
        for (unsigned i=1;i<=N-1;i++) {
        
            X[i] = X[i-1] ^ V[C[i-1]];
            // cout << C[i-1] << " " << V[C[i-1]] << " " << X[i] << endl;
            POINTS[i][0] = (double)X[i]/pow(2.0,32); // *** the actual points
            //        ^ 0 for first dimension
        }
  
        // Clean up
        delete [] V;
        delete [] X;

        // ----- Compute the remaining dimensions -----
        for(unsigned j=1;j<=D-1;j++) {
    
            // Read in parameters from file 
            unsigned d, s;
            unsigned a;
            infile >> d >> s >> a;
            unsigned *m = new unsigned [s+1];
            for (unsigned i=1;i<=s;i++) infile >> m[i];
    
            // Compute direction numbers V[1] to V[L], scaled by pow(2,32)
            unsigned *V = new unsigned [L+1];
            if (L <= s) {
                for (unsigned i=1;i<=L;i++) V[i] = m[i] << (32-i); 
            }
            else {
                for (unsigned i=1;i<=s;i++) V[i] = m[i] << (32-i); 
                for (unsigned i=s+1;i<=L;i++) {
                    V[i] = V[i-s] ^ (V[i-s] >> s); 
                    for (unsigned k=1;k<=s-1;k++){
                        V[i] ^= (((a >> (s-1-k)) & 1) * V[i-k]); 
                    }
                } 
            }
    
            // Evalulate X[0] to X[N-1], scaled by pow(2,32)
            unsigned *X = new unsigned [N];
            X[0] = 0;
            for (unsigned i=1;i<=N-1;i++){
                X[i] = X[i-1] ^ V[C[i-1]];
                POINTS[i][j] = (double)X[i]/pow(2.0,32); // *** the actual points
                //        ^ j for dimension (j+1)
            }
    
            // Clean up
            delete [] m;
            delete [] V;
            delete [] X;
        }
        delete [] C;
  
        return POINTS;
    }
    std::vector<double> piNumbers;
    int timesPiNumberCalled = 0;
    double piNumber(int Ns){

        // Empty on First Call
        if(piNumbers.empty()){
            std::ifstream piFin;
            // double pid;
            string piVal;

            if(!piFin.is_open()){
                try{
                    piFin.open("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//piNumbers.txt");
                }catch(std::ifstream::failure e){
                    std::cerr << "Open piNumbers.txt Failed\n";
                }
            }

            if(piFin.is_open()){
                while(!piFin.eof()){
                    std::getline(piFin, piVal);
                    piNumbers.push_back(std::atof(piVal.c_str()));
                }
            }
        }

        if(timesPiNumberCalled >= (int)piNumbers.size()){
            timesPiNumberCalled = 0;
        }
        
        return piNumbers[timesPiNumberCalled++];        
    }

    std::vector < std::vector < double > > latinHyperCube_Random(int Nv, int Ns, MTRand& mt){
        
        std::vector < std::vector < double > > lhsMatrix (Ns, vector<double>(Nv, 0));
        double segmentSize = 1.0/(float)Ns;
        double curSegment = 0.0;
        int a;
        double c;

        // Create Initial Matrix
        for(int i=0; i<Ns; i++){
            for(int j=0;j<Nv; j++){
                lhsMatrix[i][j] = ((i+1)-1+mt.rand())/Ns;
            }
            curSegment += segmentSize;
        }

        // Randomly match
        for(int i=0;i<Nv; i++){
            for(int j=0; j<Ns; j++){
                a = mt.randInt(Ns-1); 
                c = lhsMatrix[j][i]; 
                lhsMatrix[j][i] = lhsMatrix[a][i]; 
                lhsMatrix[a][i] = c;
            }
        }

        return lhsMatrix;
        /*
        std::vector < std::vector < double > > lhsMatrix (Ns, vector<double>(Nv, 0));
        std::vector < int > idx(Ns, 0);
        int a, b, c;

        std::srand(static_cast<unsigned>(std::time(NULL)));
        for(int i=0; i<Nv; i++){
            for(int j=0; j<Ns; j++){ idx[j] = j+1;}
            
            for(int j=0; j<Ns; j++){
                a = mt.randInt(Ns-1); 
                c = idx[i]; idx[i] = idx[a]; idx[a] = c;
            }

            for(int j=0; j<Ns; j++){ lhsMatrix[j][i] = (idx[j]-1+mt.rand())/((float)Ns);}
            for(int j=0; j<Ns; j++){ lhsMatrix[j][i] = (idx[j]-0.5)/((float)Ns);}
        }

        for(int i=0; i<Ns; i++){
            for(int j=0; j<Nv; j++){
                cout << lhsMatrix[i][j] << " ";
            }
            cout << endl;
        }

        return lhsMatrix;
        */
    }
    std::vector < std::vector < double > > descriptiveSampling_Random(int Nv, int Ns, MTRand& mt){
        
        std::vector < std::vector < double > > dsMatrix (Ns, vector<double>(Nv, 0));
        // double segmentSize = 1.0/(float)Ns;
        // double curSegment = 0.0;
        int a;
        double c;

        // Create Initial Matrix
        for(int i=0; i<Ns; i++){
            for(int j=0;j<Nv; j++){
                dsMatrix[i][j] = (i+1-0.5)/Ns;
            }
        }

        // Randomly match
        for(int i=0;i<Nv; i++){
            for(int j=0; j<Ns; j++){
                a = mt.randInt(Ns-1); 
                c = dsMatrix[j][i]; 
                dsMatrix[j][i] = dsMatrix[a][i]; 
                dsMatrix[a][i] = c;
            }
        }

        return dsMatrix;
    }
    
    // Van der Corput   - Halton in 1 dimension
    // Halton           - Use a consecutvie prime bases for each column
    // Sobol    - Faure, but uses only based 2. Each column is re-ordered based on direction numbers
    std::vector < std::vector < double > > haltonSampling(int Nd, int Ns){
        std::vector < std::vector < double > > haltonMatrix(Ns, vector<double>(Nd, 0));
        Primes p;
        double curPrime;

        for(int j=0; j<Nd; j++){
            curPrime = p.nextPrime();
            for(int i=0; i<Ns; i++){
                haltonMatrix[i][j] = Utils::corputBase(curPrime, i+1);
            } 
        }
        return haltonMatrix;
    }

    // Nd - Number of Dimensions
    // Ns - Sample, Zero-based
    std::vector < double > haltonSample(int Nd, int Ns){
        std::vector < double > haltonSample(Nd, 0.0);
        Primes p;
        double curPrime;

        for(int j=0; j<Nd; j++){
            curPrime = p.nextPrime();
            haltonSample[j] = Utils::corputBase(curPrime, Ns+1);
        }
        return haltonSample;
    }

    std::vector < std::vector < double > > hammersleySampling(int Nd, int Ns){
        std::vector < std::vector < double > > hMatrix(Ns, vector<double>(Nd, 0.0));
        Primes p;
        double curPrime;

        for(int j=1; j<Nd; j++){ // Column
            curPrime = p.nextPrime();
            for(int i=1; i<Ns; i++){ // Row
                hMatrix[i][j] = Utils::corputBase(curPrime, i-1);
            }
        }
        for(int i=0; i<Ns; i++){ // Row
            hMatrix[i][0] = i/(double)Ns;
        }
        return hMatrix;
    }

    // Nd - Number of dimensions,
    // Ns - Sample, Zero-based
    std::vector < double > hammersleySample(int Nd, int Ns, int totalSamples){
        std::vector < double > hSample(Nd, 0.0);
        Primes p;       
        double curPrime;

        hSample[0] = Ns/(double)totalSamples;

        for(int i=1; i<Nd; i++){ // Column
            curPrime = p.nextPrime();
            hSample[i] = Utils::corputBase(curPrime, Ns+1);         
        }
        
        return hSample;
    }
    std::vector < std::vector < double > > faureSampling(int Nd, int Ns){
        std::vector < std::vector < double > >  bb(Ns, std::vector<double>(Nd ,0.0));
        std::vector < std::vector < double > >  fMatrix(Ns, std::vector<double>(Nd,0.0));
        std::vector < std::vector < int > >     a(Ns, std::vector<int>(Nd,0.0));
        std::vector < std::vector < int > > G;  
        Primes prime(Nd-1);
        double curBase;
        int r;

        curBase = prime.nextPrime();
        r       = floor(log((double)Ns)/log(curBase))+1;
        G.resize(r, std::vector<int>(r, 0.0));

        for(int j=0; j<r; j++){
            for(int i=0; i<j+1; i++){
                G[j][i] = ((int)combination(j, i)) % ((int)curBase);
            }
        }

        for(int i=0; i<Ns; i++){
            for(int j=0; j<Nd; j++){
                bb[i][j] = 1.0/pow(curBase, j+1);
            }
        }

        for(int i=0; i<Ns; i++){
            for(int j=0; j<Nd; j++){
                a[i][j] = i;
            }
        }

        for(int i=0; i<(int)a.size(); i++){
            a[i] = Utils::changeBase(i, curBase, r);
        }

        for(int i=0; i<Ns; i++){
            fMatrix[i][0] = corputBase(curBase, i);
        }

        for(int i=1; i<Nd; i++){        
            a = matMult(a, G);

            for(int row=0; row<(int)a.size(); row++){
                for(int col=0; col<(int)a[row].size(); col++){
                    a[row][col] = a[row][col] % (int)curBase;
                }
            }

            for(int m=0; m<(int)fMatrix.size(); m++){
                for(int n=0; n<r; n++){
                    fMatrix[m][i] += a[m][n] * bb[m][n];
                }
            }
        }

        bb.clear(); a.clear(); G.clear();   
        return fMatrix;
    }   

    double corputBase(double base, double number){
        double h, ib;
        double i, n0, n1;

        n0  = number; 
        h   = 0;
        ib  = 1.0/base;

        while(n0 > 0){
            n1  = (int)(n0/base);
            i   = n0 - n1 * base;
            h   = h + ib * i;
            ib  = ib / base;
            n0  = n1;
        }
        return h;
    }

    std::vector< std::vector<int> > matMult(std::vector< std::vector<int> > A, std::vector< std::vector<int> > B){
        std::vector< std::vector<int> > C(A.size(), std::vector<int>(B.size(), 0));

        int sum = 0;

        for(int i=0; i<(int)A.size(); i++){
            for(int j=0; j<(int)B[0].size(); j++){
                sum = 0;       
                for(int k=0; k<(int)A[0].size(); k++){
                    sum = sum + A[i][k]*B[k][j];
                }
                C[i][j] = sum;
            }
        }
        return C;
    }

    void calculatePHEVLoad(
            double penetrationLevel, double rho, int totalVehicles, int numBuses,
            std::vector<double>& phevLoad, std::vector<double>& phevGen, MTRand& mt, pruning::PHEV_PLACEMENT phevPlacement){

        double mean, stdDev;
        double tempK, tempBC;
        double iStar;
        double chargeDischargeDistance, numClasses;
        std::vector<double> numPHEVs;
        std::vector<double> Ae, Be,         BC, K,
                            meanK,meanBC,   sigmaK,sigmaBC,
                            kMin, kMax,     bcMin, bcMax,
                            vehicleProbs,   milesDriven,
                            neededEnergy,   energyPerMile,
                            departureTime,  arrivalTime,
                            rechargeLength, rechargeVoltage,
                            rechargeCurrent,rechargeTime,
                            loadPerClass;
        std::vector < std::vector <double> > epsilon;

        numClasses = 4, chargeDischargeDistance = 40;

        numPHEVs.resize(numClasses, 0);
        meanK   .resize(numClasses, 0);         meanBC  .resize(numClasses, 0);
        sigmaK  .resize(numClasses, 0);         sigmaBC .resize(numClasses, 0);
        kMin    .resize(numClasses, 0);         kMax    .resize(numClasses, 0);
        bcMin   .resize(numClasses, 0);         bcMax   .resize(numClasses, 0);
        BC      .resize(numClasses, 0);         K       .resize(numClasses, 0);
        Ae      .resize(numClasses, 0);         Be      .resize(numClasses, 0);

        vehicleProbs    .resize(numClasses, 0);
        milesDriven     .resize(numClasses, 0);
        neededEnergy    .resize(numClasses, 0);
        energyPerMile   .resize(numClasses, 0);
        departureTime   .resize(numClasses, 0);
        arrivalTime     .resize(numClasses, 0);
        rechargeLength  .resize(numClasses, 0);
        rechargeVoltage .resize(numClasses, 0);
        rechargeCurrent .resize(numClasses, 0);
        rechargeTime    .resize(numClasses, 0);
        loadPerClass    .resize(numClasses, 0);
        epsilon         .resize(numClasses, std::vector<double>(numClasses,0));

        vehicleProbs[0] = .2; vehicleProbs[1] = .3;
        vehicleProbs[2] = .3; vehicleProbs[3] = .2;

        Ae[0]    = 0.3790; Ae[1]    = 0.4288; Ae[2]    = 0.6720; Ae[3]   = 0.8180;
        Be[0]    = 0.4541; Be[1]    = 0.4179; Be[2]    = 0.4040; Be[3]   = 0.4802;
        kMin[0]  = .2447;  kMin[1]  = .2750;  kMin[2]  = .3217;  kMin[3]  = .3224;
        kMax[0]  = 0.5976; kMax[1]  = 0.6151; kMax[2]  = 0.5428; kMax[3]  = 0.4800;
        bcMax[0] = 12;     bcMax[1] = 14;     bcMax[2] = 21;     bcMax[3] = 23;
        bcMin[0] = 8;      bcMin[1] = 10;     bcMin[2] = 17;     bcMin[3] = 19;

        for(int i=0; i<numClasses; i++){
            meanK[i]   = (kMax[i]  + kMin[i])  /2;
            meanBC[i]  = (bcMax[i] + bcMin[i]) /2;
            sigmaK[i]  = (kMax[i]  - kMin[i])  /4;
            sigmaBC[i] = (bcMax[i] - bcMin[i]) /4;
        }

        // During Each MCS Iteration
        for(int i=0; i<numClasses; i++){
            // Calculate Number of PHEVS per Class
            mean        = totalVehicles*penetrationLevel*vehicleProbs[i];
            stdDev      = .01 * mean;
            numPHEVs[i] = mt.randNorm(mean, stdDev);

            //Calculate total # of V2G?
            epsilon[0][0] = sigmaK[i]*sigmaK[i];
            epsilon[0][1] = rho*sigmaK[i]*sigmaBC[i];
            epsilon[1][0] = rho*sigmaK[i]*sigmaBC[i];
            epsilon[1][1] = sigmaBC[i]*sigmaBC[i];
            Utils::twoByTwoCholeskyDecomp(epsilon);

            // Move the following to invidiual cars
            //gsl_ran_bivariate_gaussian(r, sigmaK[i], sigmaBC[i], rho, &tempK, &tempBC);
            RandomNumbers::bivariateGaussian(mt, sigmaK[i], sigmaBC[i], rho, tempK, tempBC);
            K[i]  = meanK[i] + tempK;
            BC[i] = meanBC[i] + tempBC;

            // Calculate Miles driven per class
            //milesDriven[i] = gsl_ran_lognormal(r, 3.37, 0.5);
            milesDriven[i] = RandomNumbers::logNormal(mt, 3.37, 0.5);

            //Arrival and Departure Time
            departureTime[i] = mt.randNorm(7,  sqrt(3.0));
            arrivalTime[i]   = mt.randNorm(18, sqrt(3.0));

            // Energy Needed
            if(milesDriven[i] > chargeDischargeDistance){ // full Charge Required
                neededEnergy[i] = BC[i];
            }else{
                neededEnergy[i] = (milesDriven[i] * (Ae[i] * pow(K[i],Be[i])));
            }

            // Compute Recharge Length
            rechargeLength[i] = (24.0-arrivalTime[i]) + departureTime[i];

            // Compute Voltage needed
            if(mt.rand() < 0.70){
                rechargeVoltage[i] = 120;
                iStar = 15;
            }else{
                rechargeVoltage[i] = 240;
                iStar = 30;
            }
            // Compute current needed
            rechargeCurrent[i] = std::min((milesDriven[i]*1000)/(rechargeVoltage[i]*rechargeLength[i]), iStar);

            // Compute total power in MW in a single hour
            loadPerClass[i] = (numPHEVs[i]*rechargeVoltage[i]*rechargeCurrent[i])/1000000;
        }

        //Distributed Load
        phevLoad.clear(); phevGen.clear();
        phevLoad.resize(numBuses,0); phevGen.resize(numBuses,0);

        if(phevPlacement == pruning::PP_EVEN_ALL_BUSES){
            //Evenly at All Buses
            for(int i=0; i<numBuses; i++){
                phevLoad[i] = 0;
                for(int j=0; j<numClasses; j++){
                    phevLoad[i] += loadPerClass[j]/(double)numBuses;
                }
            }
        }else if(phevPlacement  == pruning::PP_EVEN_LOAD_BUSES){
            // Currently handles only RTS79 with 18 Buses
            for(int j=0; j<numClasses; j++){
                phevLoad[0]  += loadPerClass[j]/18.0;
                phevLoad[1]  += loadPerClass[j]/18.0;
                phevLoad[2]  += loadPerClass[j]/18.0;
                phevLoad[3]  += loadPerClass[j]/18.0;
                phevLoad[4]  += loadPerClass[j]/18.0;
                phevLoad[5]  += loadPerClass[j]/18.0;
                phevLoad[6]  += loadPerClass[j]/18.0;
                phevLoad[7]  += loadPerClass[j]/18.0;
                phevLoad[8]  += loadPerClass[j]/18.0;
                phevLoad[9]  += loadPerClass[j]/18.0;
                phevLoad[10] += loadPerClass[j]/18.0;
                phevLoad[13] += loadPerClass[j]/18.0;
                phevLoad[14] += loadPerClass[j]/18.0;
                phevLoad[15] += loadPerClass[j]/18.0;
                phevLoad[16] += loadPerClass[j]/18.0;
                phevLoad[18] += loadPerClass[j]/18.0;
                phevLoad[19] += loadPerClass[j]/18.0;
                phevLoad[20] += loadPerClass[j]/18.0;
            }
        }else if(phevPlacement  == pruning::PP_RANDOM_ALL_BUSES){
            //Evenly at All Buses
            for(int i=0; i<numBuses; i++){
                phevLoad[i] = 0;
                for(int j=0; j<numClasses; j++){
                    phevLoad[i] += loadPerClass[j]/(double)numBuses * mt.rand();
                }
            }
        }else if(phevPlacement  == pruning::PP_RANDOM_LOAD_BUSES){
            for(int j=0; j<numClasses; j++){
                phevLoad[0]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[1]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[2]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[3]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[4]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[5]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[6]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[7]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[8]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[9]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[10] += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[13] += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[14] += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[15] += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[16] += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[18] += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[19] += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[20] += loadPerClass[j]/18.0 * mt.rand();
            }
        }else if(phevPlacement == pruning::PP_FAIR_DISTRIBUTION){

            int TotalNumbOfCustomers = 1078800;
            double percentPerBus;
            for (int j = 0; j<numClasses; j++) {
                percentPerBus = 3.5 / 100.0;
                phevLoad[0] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 3.3 / 100.0;
                phevLoad[1] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 7.7 / 100.0;
                phevLoad[2] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 1.2 / 100.0;
                phevLoad[3] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 1.9 / 100.0;
                phevLoad[4] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 5.6 / 100.0;
                phevLoad[5] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 4.4 / 100.0;
                phevLoad[6] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 7.2 / 100.0;
                phevLoad[7] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 8.2 / 100.0;
                phevLoad[8] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 9.2 / 100.0;
                phevLoad[9] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 12.1 / 100.0;
                phevLoad[10] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 6.4 / 100.0;
                phevLoad[13] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 7.9 / 100.0;
                phevLoad[14] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 2.9 / 100.0;
                phevLoad[15] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 8.0 / 100.0;
                phevLoad[16] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 6.0 / 100.0;
                phevLoad[18] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 3.6 / 100.0;
                phevLoad[19] += (loadPerClass[j] * percentPerBus);

            }
        }

        //Scaling for Model
        for(int i=0; i<numBuses; i++){
            phevLoad[i] /= 100;
        }

        numPHEVs.clear();
        meanK.clear(); meanBC.clear();
        sigmaK.clear(); sigmaBC.clear();
        kMin.clear(); kMax.clear();
        bcMin.clear(); bcMax.clear();
        BC.clear(); K.clear();
        Ae.clear(); Be.clear();
        vehicleProbs.clear();
        milesDriven.clear(); neededEnergy.clear();
        energyPerMile.clear(); epsilon.clear();
        departureTime.clear(); arrivalTime.clear();
        rechargeLength.clear();
        rechargeVoltage.clear(); rechargeCurrent.clear();
        rechargeTime.clear();   loadPerClass.clear();
        epsilon.clear();

        //gsl_rng_free (r);
    }
    std::string toLower(std::string str) {
        for (unsigned int i=0;i<str.length();i++){
            str[i] = tolower(str[i]);
        }
        return str;
    }
    std::string toUpper(std::string str) {
        for (unsigned int i=0;i<str.length();i++){
            str[i] = toupper(str[i]);
        }
        return str;
    }
    std::vector<std::string> permuteCharacters(std::string topermute){

        std::vector<char> iV;
        std::vector<std::string> results;
        std::string s;

        for(unsigned int x=0; x< topermute.length(); x++){
            iV.push_back(topermute[x]);
        }

        std::sort(iV.begin(), iV.end());
        s.assign(iV.begin(), iV.end());
        results.push_back(s);

        while (std::next_permutation(iV.begin(), iV.end())){
            s.assign(iV.begin(), iV.end());
            results.push_back(s);
        }
        return results;
    }

    double sigMoid(double v){
        return 1/(1+exp(-v));
    }

    void tokenizeString(std::string str,std::vector<std::string>& tokens, const std::string& delimiter = " "){

        int pos = 0;
        std::string token;

        for(;;){

            pos = str.find(delimiter);

            if(pos == (int)std::string::npos){
                tokens.push_back(str);
                break;
            }

            token = str.substr(0, pos);
            tokens.push_back(token);
            str = str.substr(pos+1);
        }
    }

    std::vector<int> changeBase(double num, double base, int numDigits){
        std::vector<int> retValue;
    
        do{
            retValue.push_back((int)num % (int)base);
            num = (int)(num/base); 
        }while(num != 0); 
 
        while((int)retValue.size() < numDigits){
            retValue.push_back(0);
        }

        return retValue;
    }

    std::string changeBase(std::string Base, int number){
        std::stringstream ss;

        ss << Base << number;
        return ss.str();
    }