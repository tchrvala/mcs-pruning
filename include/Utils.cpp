#include "Utils.h"
#include <iostream>
#include "Eigen/Dense"

namespace Utils{

    std::vector<double> decToBin(double n, int numDigits) {
        std::vector<double> retVal;


        if(n <= 1) {
            retVal.push_back(n);
        }else{
            while(n > 0){
                retVal.push_back(floor(fmod(n, 2)));
           
                n = floor(n/2);  
            }
        }

        while((int)retVal.size() < numDigits){
            retVal.push_back(0);
        }
        reverse(retVal.begin(), retVal.end());
        return retVal;
    }
    double unitStep(double X){
        double retValue;
    
        if (X < 0.0){ retValue = 0.0;}
        else        { retValue = 1.0;}
    
        return retValue;
    }
    
    // N - Point Number
    // D - Dimension
    double *single_sobol_point(unsigned N, unsigned D){

        ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-old.111", ios::in);
        /*ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-other-0.7600", ios::in);
        ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-old-2.3900", ios::in);
        ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-old-3.7300", ios::in);
        ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-old-4.5600", ios::in);*/
        if(!infile){
            cout << "Input file containing direction numbers cannot be found!\n";
            exit(1);
        }
        // Gets rid of first line only
        char buffer[1000];
        infile.getline(buffer, 1000,'\n');

        // L = max number of bits needed 
        unsigned L = (unsigned)ceil(log((double)N)/log(2.0)); 
    
        // C[i] = index from the right of the first zero bit of i
        // for example 3 in binary 0011, so c[3]=3, which is the index of first zero from right
        // result of calculation is stored in an [N] array  
        unsigned *C = new unsigned [N];
        C[0] = 1;
        for (unsigned i=1;i<=N-1;i++) {
            C[i] = 1;
            unsigned value = i;
            while (value & 1) {
                value >>= 1;
                C[i]++;
            }
        }
    
        double *POINT = new double [D];
        for(unsigned j=0;j<D;j++){
            POINT[j]=0;
        }
    
        // Compute direction numbers V[1] to V[L], scaled by pow(2,32)
        unsigned *V = new unsigned [L+1]; 
        for (unsigned i=1;i<=L;i++){ 
            V[i] = 1 << (32-i);
        } // all m's = 1
        
        // Compute first dimension of Nth row
        // Evalulate N-1th X from 0, scaled by pow(2,32), keep changing value of X
        // N must be int >=1
        unsigned X = 0; 
        for(unsigned i=1;i<=N-1;i++){   
            X = X ^ V[C[i-1]];
        }
        POINT[0] = (double)X/pow(2.0,32);
        //    ^ first dimension of Nth row 
    
        // Clean up
        delete [] V;
    
        // ----- Compute the remaining dimensions ----- j is [1, D-1]
        for(unsigned j=1;j<=D-1;j++) {
    
            // Read in parameters from file 
            unsigned d, s;
            unsigned a;
            infile >> d >> s >> a;
            unsigned *m = new unsigned [s+1];
            for (unsigned i=1;i<=s;i++) infile >> m[i];
        
            // Compute direction numbers V[1] to V[L], scaled by pow(2,32)
            unsigned *V = new unsigned [L+1];
            if (L <= s) {
                for (unsigned i=1;i<=L;i++) V[i] = m[i] << (32-i); 
            }
            else {
                for (unsigned i=1;i<=s;i++) V[i] = m[i] << (32-i); 
                for (unsigned i=s+1;i<=L;i++) {
                    V[i] = V[i-s] ^ (V[i-s] >> s); 
                    for (unsigned k=1;k<=s-1;k++){
                        V[i] ^= (((a >> (s-1-k)) & 1) * V[i-k]); 
                    }
                } 
            }
        
            // Evalulate X[0] to X[N-1], scaled by pow(2,32)    
            unsigned X =0;
            for (unsigned i=1;i<=N-1;i++){
                X = X ^ V[C[i-1]];

            }
            POINT[j] = (double)X/pow(2.0,32); // *** the actual points
                //        ^ j for dimension (j+1)
            // Clean up
            delete [] m;
            delete [] V;
        }
        delete [] C;
  
        return POINT;   
    }
    int factorial(int n){
        int result=1;

        for (int i=1; i<=n; ++i){
            result=result*i;
        }

        return result;
    }

    // n Choose r
    int combination(int n, int r){
        return Utils::factorial(n)/(Utils::factorial(r) * Utils::factorial(n-r));
    }

    std::string vectorToString(std::vector<double> v){
        std::string result;
        char buf[1];

        result = "";
        for(unsigned int x=0; x< v.size(); x++){
            sprintf(buf, "%i", (int)v[x]);
            result += buf[0];
        }
        return result;
    }
    std::string vectorToString(std::vector<int> v){
        std::string result;

        result = "";
        for(unsigned int x=0; x< v.size(); x++){
            if(v[x] == 1){ result += '1';}
            else         { result += '0';}
        }
        return result;
    }

    std::string arrayToString(int* v, int length){
        std::string result;
        char buf[1];

        result = "";
        for(int x=0; x< length; x++){
            sprintf(buf, "%i", v[x]);
            result += buf[0];
        }
        return result;
    }

    void loadSystemData(double& pLoad, double& qLoad, int& nb, int& nt, std::string curSystem,
                            std::vector<Generator>& gens, std::vector<Line>& lines, std::vector<Bus>& buses){
        //Clear Vectors
        gens.clear(); lines.clear(); buses.clear();

        std::ifstream myFile;
        int     count = 0,
                numGens = 0;
        myFile.open(("../Data/" + curSystem).c_str());

        if (myFile.is_open()) {
            myFile >> numGens;
            myFile >> pLoad;
            myFile >> qLoad;
            myFile >> nb;
            myFile >> nt;

            std::vector<std::string> tokens;
            std::string s;

            // Generators
            for(int i = 0; i<numGens; i++){
                myFile >> s;
                Utils::tokenizeString(s,tokens,",");

                gens.push_back(Generator(
                        atof(tokens[3].c_str()),
                        atof(tokens[4].c_str()),
                        atof(tokens[5].c_str()),
                        atof(tokens[6].c_str()),
                        atof(tokens[7].c_str()),
                        atoi(tokens[0].c_str()))
                    );

                gens[i].setIndex(count);
                count++;
                tokens.clear();
            }

            // Transmission Lines
            for(int i = 0; i<nt; i++){
                myFile >> s;
                Utils::tokenizeString(s,tokens,",");

                lines.push_back(Line(
                    atoi(tokens[0].c_str()),
                    atoi(tokens[1].c_str()),
                    atoi(tokens[2].c_str()),
                    atof(tokens[3].c_str()),
                    atof(tokens[4].c_str()),
                    atof(tokens[5].c_str()),
                    atof(tokens[6].c_str()),
                    atof(tokens[7].c_str()),
                    atof(tokens[8].c_str()),
                    atof(tokens[9].c_str()),
                    atof(tokens[10].c_str()),
                    atof(tokens[11].c_str()),
                    atof(tokens[12].c_str()),
                    atof(tokens[13].c_str()))
                );
                tokens.clear();
            }

            for(int i=0; i<nb; i++){
                myFile >> s;
                Utils::tokenizeString(s,tokens,",");
                //id, type, area, zone, Pd, Qd, Gs, Bs, Vm, Va, vMin, vMax, baseKva

                buses.push_back(Bus(
                    atoi(tokens[0].c_str()),
                    atoi(tokens[1].c_str()),
                    atoi(tokens[6].c_str()),
                    atoi(tokens[10].c_str()),
                    atof(tokens[2].c_str()),
                    atof(tokens[3].c_str()),
                    atof(tokens[4].c_str()),
                    atof(tokens[5].c_str()),
                    atof(tokens[6].c_str()),
                    atof(tokens[7].c_str()),
                    atof(tokens[12].c_str()),
                    atof(tokens[11].c_str()),
                    atof(tokens[9].c_str()))
                );
                tokens.clear();
            }

            myFile.close();
        }
    }
    
    void writeLineOutages(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod, std::string classificationMethod,
            std::string pruningObj, bool useLines, bool multiObj, char* aTime, bool useLocalSearch, bool usePHEVs,bool negateFitness, double penetrationLevel,
            pruning::PHEV_PLACEMENT phevPlacement,
            std::vector < std::vector < int > > lineOutages, std::vector<Line> lines, int numThreads){


        std::stringstream   ss;
        std::ofstream       myFile;
        std::string         fName;

        ss << getBaseFileName(root, curSystem, samplingMethod, pruningMethod, classificationMethod, pruningObj, useLines, multiObj, useLocalSearch, usePHEVs,
                negateFitness, penetrationLevel, phevPlacement, numThreads);
        

        ss <<  "_" << aTime << "_LineOutageCounts.csv";
        ss >> fName;
        
        myFile.open(fName.c_str(), std::ios::trunc);
        for(unsigned int i=0; i<lines.size(); i++){
            myFile << i << " ";
        }
        myFile << std::endl;
        for(unsigned int i=0; i<lineOutages.size(); i++){
            for(unsigned int j=0; j< lineOutages[i].size(); j++){
                myFile << lineOutages[i][j] << " ";
            }
            myFile << std::endl;
        }
        myFile.close();
    }
    void writeGeneratorOutages(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod, std::string classificationMethod,
            std::string pruningObj, bool useLines, bool multiObj, char* aTime, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel,
            pruning::PHEV_PLACEMENT phevPlacement, std::vector < std::vector < int > > genOutageCounts, std::vector<Generator> gens, int numThreads) {
        std::stringstream   ss;
        std::ofstream       myFile;
        std::string         fName;

        ss << getBaseFileName(root, curSystem, samplingMethod, pruningMethod, classificationMethod, pruningObj, useLines, multiObj, useLocalSearch, usePHEVs,
                negateFitness, penetrationLevel, phevPlacement, numThreads);
        ss <<  "_" << aTime << "_GeneratorOutageCounts.csv";
        ss >> fName;
        myFile.open(fName.c_str(),std::ios::trunc);
        for(unsigned int i=0; i<gens.size(); i++){
            myFile << i << " ";
        }
        myFile << std::endl;
        for(unsigned int i=0; i<genOutageCounts.size(); i++){
            for(unsigned int j=0; j< genOutageCounts[i].size(); j++){
                myFile << genOutageCounts[i][j] << " ";
            }
            myFile << std::endl;
        }
        myFile.close();
    }

    std::string getFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod, std::string classificationMethod,
            std::string pruningObj, bool useLines, bool multiObj, char* aTime, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel,
            pruning::PHEV_PLACEMENT phevPlacement, int numThreads){

        std::stringstream ss(std::stringstream::in | std::stringstream::out);
        std::string fName;

        ss << getBaseFileName(root, curSystem, samplingMethod, pruningMethod, classificationMethod, pruningObj, useLines, multiObj, useLocalSearch, usePHEVs,
                              negateFitness, penetrationLevel, phevPlacement, numThreads);
        ss << "_" << aTime << ".csv";

        ss >> fName;
        return fName;
    }
    std::string getBaseFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod, std::string classificationMethod, 
            std::string pruningObj, bool useLines, bool multiObj, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel,
            pruning::PHEV_PLACEMENT phevPlacement, int numThreads){

        std::stringstream   ss;
        std::string fName;

        ss << root << "/" << curSystem << "/";
        ss << samplingMethod << "_"
           << pruningMethod  << "_" 
           << curSystem      << "_"
           << pruningObj;
        
        if(useLines) ss << "_LineFailures";
        else         ss << "_NoLineFailures";

        if(multiObj) ss << "_MultiObj";
        else         ss << "_SingleObj";

        ss << "_" << classificationMethod;

        if(useLocalSearch) ss << "_LocalSearch";

        if(negateFitness) ss << "_NegObj";

        if(usePHEVs){
            ss << "_PHEVs";
            ss << "_" << getPHEVPlacementString(phevPlacement);
            ss << "_" << std::fixed << std::setprecision(3) << penetrationLevel;
        }
        ss << "_OMP_" << numThreads;

        ss >> fName;
        return fName;
    }
    void getTimeStamp(char* aTime){
        time_t now;
        //char* aTime = new char[20];

        time(&now);
        struct tm * timeinfo = localtime(&now);;
        strftime(aTime, 20, "%m_%d_%Y_%H_%M_%S", timeinfo);

        //return aTime;
    }

    void twoByTwoCholeskyDecomp(std::vector<std::vector<double> >& A){

        std::vector < std::vector < double > > L(A.size(),std::vector < double > (A.size(),0));
        L[0][0] = sqrt(A[0][0]);
        L[0][1] = 0;
        L[1][0] = A[0][1]/L[0][0];
        L[1][1] = sqrt(A[1][1]-(L[1][0]*L[1][0]));
        A = L;
    }
    std::string getPruningMethodString(pruning::PRUNING_METHOD pm){
        std::string retValue;
        switch(pm){
            case pruning::PM_ACO: retValue = "ACO"; break;
            case pruning::PM_AIS: retValue = "AIS"; break;
            case pruning::PM_GA:  retValue = "GA";  break;
            case pruning::PM_MGA: retValue = "MGA"; break;
            case pruning::PM_PSO: retValue = "PSO"; break;
            case pruning::PM_RPSO:retValue = "RPSO";break;
            case pruning::PM_CFO: retValue = "CFO"; break;
            default: retValue = "NONE";
        }
        return retValue;
    }
    std::string getClassificationMethodString(pruning::CLASSIFICATION_METHOD cm){
        std::string retValue;
        switch(cm){
            case pruning::CM_NN:  retValue = "NN";  break;
            case pruning::CM_OPF: retValue = "OPF"; break;
            case pruning::CM_SVM: retValue = "SVM"; break;
            case pruning::CM_CAP: retValue = "CAP"; break;
            default: retValue = "OPF";
        }
        return retValue;
    }
    std::string getSamplingMethodString(pruning::SAMPLING_METHOD ps){
        std::string retValue;
        switch(ps){
            case pruning::SM_ACO:       retValue = "ACO";             break;
            case pruning::SM_AIS:       retValue = "AIS";             break;
            case pruning::SM_GA:        retValue = "GA";              break;
            case pruning::SM_MGA:       retValue = "MGA";             break;
            case pruning::SM_PSO:       retValue = "PSO";             break;
            case pruning::SM_RPSO:      retValue = "RPSO";            break;
            case pruning::SM_MO_PSO:    retValue = "MO_PSO";          break;
            case pruning::SM_LHS:       retValue = "LHS";             break;
            case pruning::SM_DS:        retValue = "DS";              break;
            case pruning::SM_HAL:       retValue = "HAL";             break;
            case pruning::SM_HAM:       retValue = "HAM";             break;
            case pruning::SM_FAU:       retValue = "FAU";             break;
            case pruning::SM_CFO:       retValue = "CFO";             break;
            case pruning::SM_PI:        retValue = "PI";              break;
            case pruning::SM_SOBOL:     retValue = "SOBOL";           break;
            case pruning::SM_ATV:       retValue = "ATV";             break;
            default: retValue = "MCS";
        }
        return retValue;
    }
    std::string getBooleanString(bool value){
        if(value) { return "True";}
        else      { return "False";}
    }
    std::string getPHEVPlacementString(pruning::PHEV_PLACEMENT pp){
        std::string retValue;
        switch(pp){
            case pruning::PP_NONE:              retValue = "NONE";             break;
            case pruning::PP_EVEN_ALL_BUSES:    retValue = "EvenAllBuses";     break;
            case pruning::PP_EVEN_LOAD_BUSES:   retValue = "EvenLoadBuses";    break;
            case pruning::PP_RANDOM_ALL_BUSES:  retValue = "RandomAllBuses";   break;
            case pruning::PP_RANDOM_LOAD_BUSES: retValue = "RandomLoadBuses";  break;
            case pruning::PP_FAIR_DISTRIBUTION: retValue = "FairDistribution"; break;
            default: retValue = "NONE";
        }
        return retValue;
    }
    std::string getPruningObjString(pruning::PRUNING_OBJECTIVE po){
        std::string retValue;
        switch(po){
            case pruning::PO_PROBABILITY :  retValue = "Probability";   break;
            case pruning::PO_CURTAILMENT:   retValue = "Curtailment";   break;
            case pruning::PO_COPY:          retValue = "Copy";          break;
            case pruning::PO_EXCESS:        retValue = "Excess";        break;
            case pruning::PO_ZERO:          retValue = "Zero";          break;
            case pruning::PO_TUP:           retValue = "Tup";           break;
            default: retValue = "Probability";
        }
        return retValue;
    }
    std::string getStoppingMethodString(pruning::STOPPING_METHOD sm){
        std::string retValue;
        switch(sm){
            case pruning::SM_STATES_SLOPE:  retValue = "MinStatesSlope";    break;
            case pruning::SM_STATES_PRUNED: retValue = "MaxStatesPruned";   break;
            case pruning::SM_PROB_MAX:      retValue = "MaxProbPruned";     break;
            case pruning::SM_PROB_SLOPE:    retValue = "MinProbSlope";      break;
            case pruning::SM_GENERATION:    retValue = "Generation";        break;
            default: retValue = "";
        }
        return retValue;
    }

    pruning::PRUNING_METHOD getPruningMethod(std::string s){
        pruning::PRUNING_METHOD retValue;
        s = toUpper(s);
        if(s == "ACO")      {retValue = pruning::PM_ACO;}
        else if(s == "AIS") {retValue = pruning::PM_AIS;}
        else if(s == "GA")  {retValue = pruning::PM_GA;}
        else if(s == "MGA") {retValue = pruning::PM_MGA;}
        else if(s == "PSO") {retValue = pruning::PM_PSO;}
        else if(s == "RPSO"){retValue = pruning::PM_RPSO;}
        else if(s == "CFO") {retValue = pruning::PM_CFO;}
        else                {retValue = pruning::PM_NONE;}
        return retValue;
    }
    pruning::CLASSIFICATION_METHOD getClassificationMethod(std::string s){
        pruning::CLASSIFICATION_METHOD retValue;
        s = toUpper(s);
        if(s == "NN")       {retValue = pruning::CM_NN;}
        else if(s == "OPF") {retValue = pruning::CM_OPF;}
        else if(s == "SVM") {retValue = pruning::CM_SVM;}
        else if(s == "CAP") {retValue = pruning::CM_CAP;}
        else                {retValue = pruning::CM_OPF;}
        return retValue;
    }
    pruning::SAMPLING_METHOD getSamplingMethod(std::string s){
        pruning::SAMPLING_METHOD retValue;
        s = toUpper(s);
        
        if(s == "ACO")                      {retValue = pruning::SM_ACO;}
        else if(s == "AIS")                 {retValue = pruning::SM_AIS;}
        else if(s == "GA")                  {retValue = pruning::SM_GA;}
        else if(s == "MGA")                 {retValue = pruning::SM_MGA;}
        else if(s == "PSO")                 {retValue = pruning::SM_PSO;}
        else if(s == "RPSO")                {retValue = pruning::SM_RPSO;}
        else if(s == "MO_PSO")              {retValue = pruning::SM_MO_PSO;}
        else if(s == "LHS")                 {retValue = pruning::SM_LHS;}
        else if(s == "DS")                  {retValue = pruning::SM_DS;}
        else if(s == "HAL")                 {retValue = pruning::SM_HAL;}
        else if(s == "HAM")                 {retValue = pruning::SM_HAM;}
        else if(s == "FAU")                 {retValue = pruning::SM_FAU;}
        else if(s == "CFO")                 {retValue = pruning::SM_CFO;}
        else if(s == "PI")                  {retValue = pruning::SM_PI;}
        else if(s == "SOB")                 {retValue = pruning::SM_SOBOL;}
        else if(s == "ATV")                 {retValue = pruning::SM_ATV;}
        else                                {retValue = pruning::SM_MCS;}
        return retValue;
    }
    pruning::PHEV_PLACEMENT getPHEVPlacement(std::string s){
        pruning::PHEV_PLACEMENT retValue;
        s = toUpper(s);
        if(s == "EVENALLBUSES")         {retValue = pruning::PP_EVEN_ALL_BUSES;}
        else if(s == "EVENLOADBUSES")   {retValue = pruning::PP_EVEN_LOAD_BUSES;}
        else if(s == "RANDOMALLBUSES")  {retValue = pruning::PP_RANDOM_ALL_BUSES;}
        else if(s == "RANDOMLOADBUSES") {retValue = pruning::PP_RANDOM_LOAD_BUSES;}
        else if(s == "FAIRDISTRIBUTION"){retValue = pruning::PP_FAIR_DISTRIBUTION;}
        else                            {retValue = pruning::PP_NONE;}
        return retValue;
    }
    pruning::STOPPING_METHOD getStoppingMethod(std::string s){
        pruning::STOPPING_METHOD retValue;
        s = toUpper(s);
        if(s == "MINSTATESSLOPE")       {retValue = pruning::SM_STATES_SLOPE;}
        else if(s == "MAXSTATESPRUNED") {retValue = pruning::SM_STATES_PRUNED;}
        else if(s == "MAXPROBPRUNED")   {retValue = pruning::SM_PROB_MAX;}
        else if(s == "MINPROBSLOPE")    {retValue = pruning::SM_PROB_SLOPE;}
        else if(s == "GENERATION")      {retValue = pruning::SM_GENERATION;}
        else                            {retValue = pruning::SM_GENERATION;}
        return retValue;
    }
    pruning::PRUNING_OBJECTIVE getPruningObj(std::string s){
        pruning::PRUNING_OBJECTIVE retValue;
        s = toUpper(s);
        if(s == "PROBABILITY")      {retValue = pruning::PO_PROBABILITY;}
        else if(s == "PROB")        {retValue = pruning::PO_PROBABILITY;}
        else if(s == "CURTAILMENT") {retValue = pruning::PO_CURTAILMENT;}
        else if(s == "CURT")        {retValue = pruning::PO_CURTAILMENT;}
        else if(s == "COPY")        {retValue = pruning::PO_COPY;}
        else if(s == "EXCESS")      {retValue = pruning::PO_EXCESS;}
        else if(s == "ZERO")        {retValue = pruning::PO_ZERO;}
        else if(s == "TUP")         {retValue = pruning::PO_TUP;}
        else                        {retValue = pruning::PO_PROBABILITY;}
        
        return retValue;
    }

    void printVector(std::vector < std::vector < double > >& v, std::string title, int precision){
        std::cout << title << std::endl;
        for(int i=0; i<(int)v.size(); i++){
            for(int j=0; j<(int)v[i].size(); j++){
                std::cout << setprecision(precision) << v[i][j]<< " ";
            }
            std::cout << std::endl;
        }
    }
    void printVector(std::vector < std::vector < std::vector < double > > >& v, std::string title, int precision){
        std::cout << title << std::endl;
        for(int i=0; i<(int)v.size(); i++){
            for(int j=0; j<(int)v[i].size(); j++){
                for(int k=0; k<(int)v[i][j].size(); k++){
                    std::cout << setprecision(precision) << v[i][j][k] << " ";
                }
            }
            std::cout << std::endl;
        }
    }

    void setUsage(AnyOption* opt){
        // General
        opt->addUsage( "" );
        opt->addUsage( "Usage: " );
        opt->addUsage( "" );
        opt->addUsage( "--help              Prints this help");
        opt->addUsage( "" );

        // Parallelization
        opt->addUsage("--numThreads        # of threads to use with OpenMP");   
        opt->addUsage("--batchSize         # of samples per batch used in MCS Sampling with OpenMP or CUDA");
        
        // Parallel Batch OpenMP
        opt->addUsage("--genThreads        For use with OpenMP Pipeline. Number of Generation Threads.");
        opt->addUsage("--classThreads      For use with OpenMP Pipeline. Number of Classification Threads.");

        // Power System Specific:wq
        opt->addUsage("--system            RTS79, MRTS, or RTS96. Defaults to RTS79");
        opt->addUsage("--useLines          Use Transmission Lines");
        opt->addUsage("--usePHEVs          Use PHEV formulation");
        opt->addUsage("--lineAdj           Multiplied with line FOR. Defaults to 1.0");
        opt->addUsage("--saveGenOutages    Save Generator Outage Stats");
        opt->addUsage("--saveLineOutages   Save Line Outage Stats");
        opt->addUsage("" );

        // PHEV Specific
        opt->addUsage("--penetration       Penetration level for PHEVs");
        opt->addUsage("--rho               Rho value for PHEVs");
        opt->addUsage("--totalVehicles     Total Vehicles for PHEVs");
        opt->addUsage("--placement         PHEV Placement - EvenAllBuses, EvenLoadBuses, RandomAllBuses, RandomLoadBuses, FairDistribution");
        opt->addUsage("" );

        // Classification
        opt->addUsage("--classifier        CAP, SVM, NN, or OPF. Defaults to OPF");
        opt->addUsage("" );
        
        // Sampling
        opt->addUsage("--sampler           MCS, LHS, DS, HAL, HAM, FAU, SOB, or PI. Defaults to MCS.");
        opt->addUsage("--samplingPop       Sampling population size. Defaults to 25.");
        opt->addUsage("--samplingGen       Sampling number of generations. Defaults to 25.");
        opt->addUsage("--sigma             Tolerance for convergance");
        opt->addUsage("--useMOSampler      Use Multi-Objective Sampler");
        opt->addUsage("--numSamples        Number of experiments for LHS");
        opt->addUsage("" );

        // Pruning
        opt->addUsage("--pruner            MCS, AIS, GA, MGA, PSO, or ACO. Defaults to MCS.");
        opt->addUsage("--useMOPruner       Use Multi-Objective Pruner");
        opt->addUsage("--useWeightedMO     Use Weighted version of Multi-Objective Pruner");
        opt->addUsage("--useParetoMO       Use Pareto version of Multi-Objective Pruner");
        opt->addUsage("--useLocalSearch    Use Local Search Algorithm");
        opt->addUsage("" );
        opt->addUsage("--logConvergence    Log Convergence Characteristics");
        opt->addUsage("--negateFitness     Negate Fitness Function");
        opt->addUsage("--pruningObj        Prob, Curt, Excess, or Copy. Defaults to Prob");
        opt->addUsage("" );
        opt->addUsage("--pruningPopMin     Min population sizeDefaults to 25.");
        opt->addUsage("--pruningPopMax     Max population sizeDefaults to 25.");
        opt->addUsage("--pruningPopStep    Incremental value from popMin to popMaxDefaults to 5.");
        opt->addUsage("" );
        opt->addUsage("--pruningGenMin     Min number of generationsDefaults to 25.");
        opt->addUsage("--pruningGenMax     Max number of generationsDefaults to 25.");
        opt->addUsage("--pruningGenStep    Incremental value from genMin to genMax. Defaults to 5.");
        opt->addUsage("" );
        opt->addUsage("--stoppingMethod    MaxProbPruned, MinProbSlope, MaxStatesPruned, MinStatesSlope, or Generation. Defaults to Generation.");
        opt->addUsage("--stoppingValue     Used in conjection with stoppingMethod.");
        opt->addUsage("--trials            Trials per run");

    }
    void setOptions(AnyOption* opt){
        // Flags
        opt->setFlag("help", 'h');

        // Parallel
        opt->setOption("batchSize"); 
        opt->setOption("numThreads");

        // Parallel Batch OpenMP
        opt->setOption("genThreads");
        opt->setOption("classThreads");

        // Power System
        opt->setOption("system");
        opt->setOption("lineAdj");
        opt->setFlag("useLines");
        opt->setFlag("usePHEVs");
        opt->setFlag("saveGenOutages");
        opt->setFlag("saveLineOutages");

        // PHEV Options
        opt->setOption("penetration");
        opt->setOption("rho");
        opt->setOption("totalVehicles");
        opt->setOption("placement");

        // Classifying
        opt->setOption("classifier");

        // Sampling
        opt->setOption("sampler");      
        opt->setOption("samplingPop");  
        opt->setOption("samplingGen");  
        opt->setOption("sigma");
        opt->setOption("numSamples");

        // Pruning
        opt->setFlag("logConvergence");
        opt->setFlag("negateFitness");
        opt->setFlag("useMOPruner");
        opt->setFlag("useWeightedMO");
        opt->setFlag("useParetoMO");
        opt->setFlag("useLocalSearch");

        opt->setOption("pruner");
        opt->setOption("pruningObj");
        opt->setOption("trials");
        opt->setOption("stoppingMethod");
        opt->setOption("stoppingValue");
        opt->setOption("pruningPopMin"); opt->setOption("pruningPopMax"); opt->setOption("pruningPopStep");
        opt->setOption("pruningGenMin"); opt->setOption("pruningGenMax"); opt->setOption("pruningGenStep");
    }
}
