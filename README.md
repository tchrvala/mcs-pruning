# MCS-PS #
:
The probabilistic reliability evaluation of composite power systems is a complicated, computation intensive, and combinatorial task. As such evaluation may suffer from issues regarding high dimensionality that lead to an increased need for computational resources. Non-Sequential Monte Carlo Simulation (MCS) is often used to evaluate the reliability of power systems for various reasons. This software implements Non-Sequential MCS with multiple variants to improve performance. 
s
## Related Publications ##
1. **R. Green**, L. Wang, and M. Alam, Intelligent State Space Pruning for Monte Carlo Simulation with Applications in Composite Power System Reliability. Engineering Applications of Artificial Intelligence, vol. 26, no. 7, pp. 1707-1724, 2013, 10.1016/j.engappai.2013.03.006
    
2. **R. Green**, L. Wang, and M. Alam, Applications and Trends of High Performance Computing for Electric Power Systems: Focusing on Smart Grid, IEEE Transactions on Smart Grid, vol. 4, no. 2, pp. 922-931, June 2013.

3. **R. Green**, L. Wang, and M. Alam, "Intelligent State Space Pruning with Local Search for Power System Reliability Evaluation," IEEE Power and Energy Society Innovative Smart Grid Technologies Europe, Berlin, Germany, October 2012.

4. **R. Green**, L. Wang, M. Alam, and C. Singh, "Evaluating the Impact of Low Discrepancy Sequences on the Probabilistic Evaluation of Composite Power System Reliability," IEEE Power and Energy Society General Meeting 2012, San Diego, California, July 2012.

5. **R. Green**, L. Wang, M. Alam, and C. Singh, "Latin Hypercube Sampling for the Probabilistic Evaluation of Composite Power System Reliability," Probabilistic Methods Applied to Power Systems, Istanbul, Turkey, June 2012.

6. **R. Green**, L. Wang, M. Alam, and S. S. S. R. Depuru, "An Examination of Artificial Immune System Optimization in Intelligent State Space Pruning," North American Power Symposium 2011, Boston, Massachusetts, August 2011.

7. **R. Green**, L. Wang, M. Alam, and S. S. S. R. Depuru, "Evaluating the Impact of Plug-in Hybrid Electric Vehicles on Composite Power System Reliability," North American Power Symposium 2011, Boston, Massachusetts, August 2011.

8. **R. Green**, L. Wang, and M. Alam, “Composite power system reliability evaluation using support vector machines on a multicore platform,” IEEE International Joint Conference on Neural Networks, San Jose, California, August 2011.

9. **R. Green**, L. Wang, M. Alam, and C. Singh, "Intelligent State Space Pruning Using Multi-Objective PSO for Reliability Analysis of Composite Power Systems: Observations, Analyses, and Impacts," IEEE Power and Energy Society General Meeting 2011 (PESGM 2011), Detroit, Michigan, July 2011. 

10. **R. Green**, L. Wang, M. Alam, and C. Singh, "State space pruning for Reliability Evaluation using Binary Particle Swarm Optimization," Power Systems Exhibition and Conference, Phoenix, Arizona, March 2011.
     
11. **R. Green**, L. Wang, M. Alam, and C. Singh, "Intelligent and Parallel State Space Pruning for Power System Reliability Analysis Using MPI on a Multicore Platform," Second Conference on Innovative Smart Grid Technologies (ISGT 2011), Anaheim, California, January 2011.

12. **R. Green**, L. Wang, Z. Wang, M. Alam, and C. Singh, "Power System Reliability Assessment Using Intelligent State Space Pruning Techniques: A Comparative Study” 2010 Conference on Power System Technology, Hangzou China, October 2010.

13. **R. Green**, L. Wang, and C. Singh, "State Space Pruning for Power System Reliability Evaluation using Genetic Algorithms," in Proceedings of the IEEE PES General Meeting, Minneapolis, MN, July 2010.
 
## Features ##

### Sampling ###
1. MCS Sampler
2. PSO Sampler
3. Latin Hypercube Sampler
4. Low Discrepancy Sequence Sampler
5. Descriptive Sampling Sampler
6. CFO Sampler
7. Antithetic Variate Sampling
8. Batch Sampling w/ OpenMP

### Classification ###
1. DC-OPF State Classification
2. Capacity State Classification
3. Neural Network State Classification
4. Support Vector Machine (SVM) State Classification

## Building the Software ##
### Dependencies ###
1. lpsolve
2. libsvm

### Instructions ###
1. Change directory to "Default"
2. Type "make all"

## Contributing to the Code ##


## Adding a Classifier ###


## Adding a Sampler ###
