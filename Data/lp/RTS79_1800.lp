/* Please email questions, comments, and significant results to: Robert.C.Green@gmail.com.  Thanks!

 (c) 2009-2010 Zhu Wang, Robert Green

 ALL RIGHTS RESERVED WORLDWIDE

 THIS PROGRAM IS FREEWARE.  IT MAY BE COPIED AND
 DISTRIBUTED WITHOUT LIMITATION AS LONG AS THIS
 COPYRIGHT NOTICE AND REFERENCE
 INFORMATION BELOW ARE INCLUDED WITHOUT MODIFICATION,
 AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
 INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR
 OTHER PRODUCTS.

 This code implements the IEEE-RTS79 model DC Optimal Power FLow using LPSolve

 References concerning this code include:
 1 - R. Green, L. Wang, Z. Wang, M. Alam, and C. Singh, “Power System Reliability Assessment Using Intelligent State Space Pruning Techniques: A Comparative Study” Submitted to 2010 Conference on Power System Technology, Hangzou China, October 2010.
 2 - R. Green, L. Wang, M. Alam, and C. Singh, “State space pruning for Reliability Evaluation using Binary Particle Swarm Optimization,” Submitted to Hawaii International Conference on System Sciences,University of Hawaii at Manoa, January 2011.
 3 - R. Green, L. Wang, and C. Singh, “State space pruning for power system reliability evaluation using genetic algorithms,” IEEE Power & Energy Society General Meeting 2010, Minneapolis, MN, July 2010.
 4 - R. Green, Z. Wang, L. Wang, M. Alam, and C. Singh, “Evaluation of loss of LOAD probability for power systems using intelligent search based state space pruning,” The 11th International Conference on Probabilistic Methods Applied to Power Systems, Singapore, June 2010*/

/* Objective function */
min: C1 +C2 +C3 +C4 +C5 +C6 +C7 +C8 +C9 +C10 +C11 +C12 +C13 +C14 +C15 +C16 +C18 +C19 +C20 +C21 +C22 +C23 +C24;

LOAD_1: 88.512T1 	 -71.942T2 	-4.7348T3  -11.834T5 						-1G1  -1C1 	= -0.68;
LOAD_2: -71.942T1 	 +85.043T2 	-7.8927T4  -5.2083T6 						-1G2  -1C2 	= -0.61;
LOAD_3: -4.7348T1 	 +24.881T3  -8.4034T9  -11.743T24  					    -1G3  -1C3 	= -1.14;
LOAD_4: -7.8927T2 	 +17.536T4 	-9.6432T9 									-1G4  -1C4 	= -0.47;
LOAD_5: -11.834T1 	 +23.159T5 	-11.325T10  								-1G5  -1C5 	= -0.45;
LOAD_6: -5.2083T2 	 +21.737T6 	-16.529T10 									-1G6  -1C6  = -0.86;
LOAD_7: 16.287T7 	 -16.287T8  											-1G7  -1C7 	= -0.79;
LOAD_8: -16.287T7 	 +28.401T8 	-6.0569T9  -6.0569T10 						-1G8  -1C8 	= -1.08;
LOAD_9: -8.4034T3 	 -9.6432T4	-6.0569T8  +47.247T9 -11.572T11 -11.572T12 	-1G9  -1C9 	= -1.11;
LOAD_10: -11.325T5 	 -16.529T6 	-6.0569T8  +57.397T10 -11.743T11 -11.743T12 -1G10 -1C10 = -1.23;
LOAD_11: -11.572T9 	 -11.743T10 +68.246T11 -21.008T13 -23.923T14 			-1G11 -1C11	= 0;
LOAD_12: -11.572T9 	 -11.743T10 +54.675T12 -21.008T13 -10.352T23 			-1G12 -1C12	= 0;
LOAD_13: -21.008T11	 -21.008T12 +53.578T13 -11.561T23 						-1G13 -1C13 = -1.67;
LOAD_14: -23.923T11  +49.63T14 	-25.707T16 									-1G14 -1C14 = -1.23;
LOAD_15: +117.89T15  -57.803T16 -40.816T21 -19.268T24 					    -1G15 -1C15 = -2.00
LOAD_16: -25.707T14  -57.803T15 +165.41T16 -38.61T17 -43.29T19 			    -1G16 -1C16 = -0.63;
LOAD_17: -38.61T16 	 +117.55T17 -69.444T18 -9.4967T22 						-1G17 -1C17	= 0;
LOAD_18: -69.444T17  +146.66T18 -77.22T21 								    -1G18 -1C18 = -2.10;
LOAD_19: -43.29T16 	 +93.795T19 -50.505T20									-1G19 -1C19 = -1.14;
LOAD_20: -50.505T19  +143.1T20 	-92.593T23 								    -1G20 -1C20 = -0.81;
LOAD_21: -40.816T15  -77.22T18 	+132.79T21 -14.749T22 					    -1G21 -1C21	= 0;
LOAD_22: -9.4967T17	 -14.749T21 +24.246T22 									-1G22 -1C22	= 0;
LOAD_23: -10.352T12  -11.561T13 -92.593T20 +114.51T23 					    -1G23 -1C23	= 0;
LOAD_24: -11.743T3 	-19.268T15 	+31.011T24 									-1G24 -1C24	= 0;

/* CURTAILMENT */
CURTAIL_1: +1C1     <= 0.68;
CURTAIL_2: +1C2     <= 0.61;
CURTAIL_3: +1C3     <= 1.14;
CURTAIL_4: +1C4     <= 0.47;
CURTAIL_5: +1C5     <= 0.45;
CURTAIL_6: +1C6     <= 0.86;
CURTAIL_7: +1C7     <= 0.79;
CURTAIL_8: +1C8     <= 1.08;
CURTAIL_9: +1C9     <= 1.11;
CURTAIL_10: +1C10   <= 1.23;
CURTAIL_11: +1C11   <= 0;
CURTAIL_12: +1C12   <= 0;
CURTAIL_13: +1C13   <= 1.67;
CURTAIL_14: +1C14   <= 1.23;
CURTAIL_15: +1C15   <= 2.00;
CURTAIL_16: +1C16   <= 0.63;
CURTAIL_17: +1C17   <= 0;
CURTAIL_18: +1C18   <= 2.10;
CURTAIL_19: +1C19   <= 1.14;
CURTAIL_20: +1C20   <= 0.81;
CURTAIL_21: +1C21   <= 0;
CURTAIL_22: +1C22   <= 0;
CURTAIL_23: +1C23   <= 0;
CURTAIL_24: +1C24   <= 0;

/* Generation */
+1G1 	<= 1.92;
+1G2 	<= 1.92;
+1G3 	<= 0;
+1G4 	<= 0;
+1G5 	<= 0;
+1G6 	<= 0;
+1G7 	<= 3.00;
+1G8 	<= 0;
+1G9 	<= 0;
+1G10 	<= 0;
+1G11 	<= 0;
+1G12 	<= 0;
+1G13 	<= 5.91;
+1G14 	<= 0;
+1G15 	<= 2.15;
+1G16 	<= 1.55;
+1G17 	<= 0;
+1G18 	<= 4.00;
+1G19 	<= 0;
+1G20 	<= 0;
+1G21 	<= 4.00;
+1G22 	<= 3.00;
+1G23 	<= 6.60;
+1G24 	<= 0;

/* Flow */
F1_1: +71.942T1  -71.942T2     <= 1.93;
F1_2: +4.7348T1  -4.7348T3     <= 2.08;
F1_3: +11.834T1  -11.834T5     <= 2.08;
F1_4: +7.8927T2  -7.8927T4     <= 2.08;
F1_5: +5.2083T2  -5.2083T6     <= 2.08;
F1_6: +8.4034T3  -8.4034T9     <= 2.08;
F1_7: +11.743T3  -11.743T24    <= 5.10;
F1_8: +9.6432T4  -9.6432T9     <= 2.08;
F1_9: +11.325T5  -11.325T10    <= 2.08;
F1_10: +16.529T6  -16.529T10   <= 1.93;
F1_11: +16.287T7  -16.287T8    <= 2.08;
F1_12: +6.0569T8  -6.0569T9    <= 2.08;
F1_13: +6.0569T8  -6.0569T10   <= 2.08;
F1_14: +11.572T9  -11.572T11   <= 5.10;
F1_15: +11.572T9  -11.572T12   <= 5.10;
F1_16: +11.743T10 -11.743T11   <= 5.10;
F1_17: +11.743T10 -11.743T12   <= 5.10;
F1_18: +21.008T11 -21.008T13   <= 6.00;
F1_19: +23.923T11 -23.923T14   <= 6.00;
F1_20: +21.008T12 -21.008T13   <= 6.00;
F1_21: +10.352T12 -10.352T23   <= 6.00;
F1_22: +11.561T13 -11.561T23   <= 6.00;
F1_23: +25.707T14 -25.707T16   <= 6.00;
F1_24: +57.803T15 -57.803T16   <= 6.00;
F1_25: +20.408T15 -20.408T21   <= 6.00;
F1_26: +20.408T15 -20.408T21   <= 6.00;
F1_27: +19.268T15 -19.268T24   <= 6.00;
F1_28: +38.61T16  -38.61T17    <= 6.00;
F1_29: +43.29T16  -43.29T19    <= 6.00;
F1_30: +69.444T17 -69.444T18   <= 6.00;
F1_31: +9.4967T17 -9.4967T22   <= 6.00;
F1_32: +38.61T18  -38.61T21    <= 6.00;
F1_33: +38.61T18  -38.61T21    <= 6.00;
F1_34: +25.253T19 -25.253T20   <= 6.00;
F1_35: +25.253T19 -25.253T20   <= 6.00;
F1_36: +46.296T20 -46.296T23   <= 6.00;
F1_37: +46.296T20 -46.296T23   <= 6.00;
F1_38: +14.749T21 -14.749T22   <= 6.00;

F2_1: -71.942T1  +71.942T2      <= 1.93;
F2_2: -4.7348T1  +4.7348T3      <= 2.08;
F2_3: -11.834T1  +11.834T5      <= 2.08;
F2_4: -7.8927T2  +7.8927T4      <= 2.08;
F2_5: -5.2083T2  +5.2083T6      <= 2.08;
F2_6: -8.4034T3  +8.4034T9      <= 2.08;
F2_7: -11.743T3  +11.743T24     <= 5.10;
F2_8: -9.6432T4  +9.6432T9      <= 2.08;
F2_9: -11.325T5  +11.325T10     <= 2.08;
F2_10: -16.529T6  +16.529T10 	<= 1.93;
F2_11: -16.287T7  +16.287T8  	<= 2.08;
F2_12: -6.0569T8  +6.0569T9  	<= 2.08;
F2_13: -6.0569T8  +6.0569T10 	<= 2.08;
F2_14: -11.572T9  +11.572T11 	<= 5.10;
F2_15: -11.572T9  +11.572T12 	<= 5.10;
F2_16: -11.743T10 +11.743T11 	<= 5.10;
F2_17: -11.743T10 +11.743T12 	<= 5.10;
F2_18: -21.008T11 +21.008T13 	<= 6.00;
F2_19: -23.923T11 +23.923T14 	<= 6.00;
F2_20: -21.008T12 +21.008T13 	<= 6.00;
F2_21: -10.352T12 +10.352T23 	<= 6.00;
F2_22: -11.561T13 +11.561T23 	<= 6.00;
F2_23: -25.707T14 +25.707T16 	<= 6.00;
F2_24: -57.803T15 +57.803T16 	<= 6.00;
F2_25: -20.408T15 +20.408T21 	<= 6.00;
F2_26: -20.408T15 +20.408T21 	<= 6.00;
F2_27: -19.268T15 +19.268T24 	<= 6.00;
F2_28: -38.61T16  +38.61T17  	<= 6.00;
F2_29: -43.29T16  +43.29T19  	<= 6.00;
F2_30: -69.444T17 +69.444T18 	<= 6.00;
F2_31: -9.4967T17 +9.4967T22 	<= 6.00;
F2_32: -38.61T18  +38.61T21  	<= 6.00;
F2_33: -38.61T18  +38.61T21  	<= 6.00;
F2_34: -25.253T19 +25.253T20 	<= 6.00;
F2_35: -25.253T19 +25.253T20 	<= 6.00;
F2_36: -46.296T20 +46.296T23 	<= 6.00;
F2_37: -46.296T20 +46.296T23 	<= 6.00;
F2_38: -14.749T21 +14.749T22 	<= 6.00;

/* Variable bounds */
G1  >= 0;
G2  >= 0;
G3  >= 0;
G4  >= 0;
G5  >= 0;
G6  >= 0;
G7  >= 0;
G8  >= 0;
G9  >= 0;
G10 >= 0;
G11 >= 0;
G12 >= 0;
G13 >= 0;
G14 >= 0;
G15 >= 0;
G16 >= 0;
G17 >= 0;
G18 >= 0;
G19 >= 0;
G20 >= 0;
G21 >= 0;
G22 >= 0;
G23 >= 0;
G24 >= 0;

C1  >= 0;
C2  >= 0;
C3  >= 0;
C4  >= 0;
C5  >= 0;
C6  >= 0;
C7  >= 0;
C8  >= 0;
C9  >= 0;
C10 >= 0;
C11 >= 0;
C12 >= 0;
C13 >= 0;
C14 >= 0;
C15 >= 0;
C16 >= 0;
C17 >= 0;
C18 >= 0;
C19 >= 0;
C20 >= 0;
C21 >= 0;
C22 >= 0;
C23 >= 0;
C24 >= 0;
